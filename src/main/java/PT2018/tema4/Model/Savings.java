package PT2018.tema4.Model;


import java.io.Serializable;

public class Savings extends Account implements Serializable{


    //The saving account allows a
    //single large sum deposit and withdrawal and computes an interest during the deposit
    // period.


    private float perioadaDepunere =30 ;
    private boolean potSaRetrag  = true;

    public Savings(String name, int s, boolean acTyp,float perioadaDepunere) {
        super(name, s, acTyp);
        this.perioadaDepunere = perioadaDepunere;
    }

    public void plusDobanda(){
        int NR_ZILE_AN = 365;
        float DOBANDA = 16;
        float aux = getSum();
        float calcul = aux+ getSum()*(DOBANDA /100) * (perioadaDepunere / NR_ZILE_AN);
        System.out.println("dupa dobanda "+ calcul);
        setSum(calcul );

    }


    @Override
    public void retragereNumerar(int deRetras) {
        if(potSaRetrag) {
            int RETRAGERE_MINIMA = 1000;
            if (deRetras < RETRAGERE_MINIMA) {
                setMess("Need to withdraw more moneyyy");
            }
            else {
                potSaRetrag = false;
                setMess("withdraw done");
                 super.retragereNumerar(deRetras);
            }
        } else setMess("Already made a withdraw");
    }

    public boolean getPotsaRetrag(){
        return potSaRetrag;
    }

    public float getPerioadaDepunere() {
        return perioadaDepunere;
    }

    public void setPerioadaDepunere(int perioadaDepunere) {
        this.perioadaDepunere = perioadaDepunere;
    }

}
