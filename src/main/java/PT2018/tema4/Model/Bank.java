package PT2018.tema4.Model;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Bank  implements BankProc, Serializable{

    //add/remove persons, add/remove holder associated accounts,     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1 pana aici facut, hai sa testam
    // read/write accounts data etc). Specify the pre and post conditions for the
    // interface methods.

    private HashMap<Person, ArrayList<Account>> manage = new HashMap<>();

    private ArrayList<Object> personArrayList = new ArrayList<>();

    FileInputStream fileInputStream;
    FileOutputStream fileOutputStream;
    ObjectInputStream in;
    ObjectOutputStream iesire;

    private String mess;

    public Bank() throws IOException, ClassNotFoundException {
       deserializeCitire();

    }


    ArrayList<Object> makeObjList = new ArrayList<>();
    public ArrayList<Object> getMakeObjList() {
        return (ArrayList<Object>) personArrayList;
    }


    public ArrayList<Object> getPersonArrayList() throws IOException, ClassNotFoundException {
        deserializeCitire();
        serializeScriere();
        assert (personArrayList != null);
        return personArrayList;
    }

    public Person addPerson(String name, String email) throws IOException {
        try {
            deserializeCitire();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Person p = new Person(name, email);
        assert (p!=null);
        ArrayList<Account> acc = new ArrayList<>();
        manage.put(p,acc );
        personArrayList.add(p);
       serializeScriere();
        return p;
    }

    public void removePerson (Person pii) throws IOException, ClassNotFoundException {  ///nu pot fi mai multe pers cu same name and email
       deserializeCitire();
        assert (pii !=null);
        manage.remove(pii);
        personArrayList.remove(pii);


        serializeScriere();
    }


    public void addSum(Person p, Account a, float sum) throws IOException {
        try {
            deserializeCitire();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        for(Account at : manage.get(p)){
           if(a.equals(at)){
               if(!a.getISSavin()){
                   at.depunereNumerart(sum);
                   setMess("Ok depozit");
               }else
                   setMess("Cannot deposit");

           }
       }
       serializeScriere();
    }


    public void retragere(Person p, Account a, int sum) throws IOException {
        try {
            deserializeCitire();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        for(Account at : manage.get(p)){
            if(a.equals(at)){
                if(a.getISSavin()){
                    Savings s = (Savings)a;
                    if(s.getPotsaRetrag()){
                        at.retragereNumerar(sum);
                        setMess("Ok retras");
                    }else  setMess("Already withdraw once");
                }else{
                    at.retragereNumerar(sum);
                    setMess("Ok retras");
                }

            }
        }
        serializeScriere();
    }




    public Account addSAVINGSAccToPers(Person p, String name, int s, boolean acTyp, float perioadaDepunere) throws IOException, ClassNotFoundException {

        if(acTyp == true){
            try {
                deserializeCitire();
                assert (p != null);
                Savings savings = new Savings(name, s, acTyp, 30);
                manage.get(p).add(savings);
                serializeScriere();
                savings.registerObserver(p);
                return savings;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        else{

            deserializeCitire();
            assert (p != null);
            Spending spending = new Spending(name, s, acTyp);
            manage.get(p).add(spending);
            serializeScriere();
            spending.registerObserver(p);
            return spending;
        }



    }



    public ArrayList<Account> getSpecificPersonALLAcc(Person p) throws IOException, ClassNotFoundException {
        assert (p != null);
        deserializeCitire();
        return  manage.get(p);
    }

    public void removeAccountFromPerson(Person p, Account a) throws IOException, ClassNotFoundException {
        assert (p !=null && a != null);
        deserializeCitire();
       Iterator<Account> accountIterator = manage.get(p).iterator();
       while (accountIterator.hasNext()){
           Account aux = accountIterator.next();
           if(aux.equals(a)){
               aux.deleteObserver(p);
               accountIterator.remove();
               serializeScriere();
               mess = "Acc removed";
           }
           else {
               mess="No such acc to remove";
           }
       }

    }


    public void getAccountData(Person p, Account a) throws IOException, ClassNotFoundException {
        assert (p !=null && a != null);
        deserializeCitire();
        Iterator<Account> accountIterator = manage.get(p).iterator();
        while (accountIterator.hasNext()){
            Account aux = accountIterator.next();
            if(aux.equals(a)){
                mess=" Owner is: "+p.getName() + " Account name is ";
              mess += a.getAccName();
              mess+=" With remaining sum ";
              mess+=a.getSum();
            }
            else {
                mess="No such acc";
            }
        }
        System.out.println(mess);
    }

    public void writeAccountData(Person p, Account a, String fieldToUpdate, String newValue) throws IOException, ClassNotFoundException {
        assert (p !=null && a != null);
        deserializeCitire();
        Iterator<Account> accountIterator = manage.get(p).iterator();
        while (accountIterator.hasNext()){
            Account aux = accountIterator.next();
            if(aux.equals(a)){
                if(fieldToUpdate == "name") {
                    aux.setAccName(newValue);
                    mess="Update ok";
                } else if(fieldToUpdate == "sum"){
                    aux.setSum(Integer.parseInt(newValue));
                    mess="Update ok";
                }

            }
            else {
                mess="No such acc";
            }
        }
        serializeScriere();
        System.out.println(mess);
    }



    public void updatePerson(Person p, int col, String newValue) throws IOException, ClassNotFoundException {
        assert (p !=null );
        deserializeCitire();
        ArrayList<Account> acc =getSpecificPersonALLAcc(p);
        Person pu = p;
        if(col == 0) //modific numele
        {
            pu.setName(newValue);
        }
        if(col == 1){
            pu.setEmail(newValue);
        }
        ArrayList<Account> accounts = manage.get(p);
        personArrayList.remove(p);
        personArrayList.add(pu);
       // removePerson(p);
        manage.remove(p);
        manage.put(pu,accounts);



       // manage.replace(p, acc);

        serializeScriere();
        System.out.println(mess);
    }


    public void serializeScriere() throws IOException {
            fileOutputStream = new FileOutputStream("ffile.ser");
            iesire = new ObjectOutputStream(fileOutputStream);
            iesire.writeObject(this.manage);
            iesire.writeObject(this.personArrayList);
            iesire.close();
    }

    public void deserializeCitire() throws IOException, ClassNotFoundException {
            fileInputStream = new FileInputStream("ffile.ser");
            in = new ObjectInputStream(fileInputStream);
            this.manage = (HashMap<Person,ArrayList<Account>>) in.readObject();
            this.personArrayList = (ArrayList<Object>)in.readObject();
            in.close();
    }
    

    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }
}
