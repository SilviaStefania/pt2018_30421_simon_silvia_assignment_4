package PT2018.tema4.Model;

import java.io.Serializable;
import java.util.Observer;
import java.util.Observable;
import java.util.ArrayList;

public class Account extends Observable implements Serializable{




    //The saving account allows a
    //single large sum deposit and withdrawal and computes an interest during the deposit
   // period. The spending account allows several deposits and withdrawals, but does not
   // compute any interest.


    private String accName;
    private float sum;
    private boolean isSavings;
    private final int DEPUNERE_MINIMA = 1000;
    private String mess;
    private ArrayList<Observer> observers = new ArrayList<>();

    public Account(String name, float s,  boolean isSavings){
        super();
        this.isSavings = isSavings;
        if(this.isSavings) {
            if (s < DEPUNERE_MINIMA) {
                mess = "Put a larger amount of moneyyy";
                System.out.println(mess);
            } else {
                accName = name;
                sum = s;
                mess = "Insert moneyyy ok";
                System.out.println(mess);
            }
        }else{
            accName = name;
            sum = s;
            mess = "Insert moneyyy ok";
            System.out.println(mess);
        }

    }

    public ArrayList<Observer> getObservers(){
        return observers;
    }

public boolean getISSavin(){
        return isSavings;
}


    public void setObservers(ArrayList<Observer> observers){
        this.observers = observers;
    }

    public void notifyObserves(Observable observable, String accName){
        System.out.println("Notifying to the person when account operation has been made ");
        for (Observer ob: observers)
            ob.update(observable,accName);

    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(this.getClass() != obj.getClass()) return false;
        Account acca = (Account) obj;
        return  (accName.equals(acca.accName )) ; //retrun false daca ambele sutn diferite
    }

    public void registerObserver(Observer observer){
        observers.add(observer);
    }

    public void removeObserver(Observer observer){
        observers.remove(observer);
    }

    public String getMess() {
        return mess;
    }

    public void retragereNumerar(int deRetras){
        sum = sum - deRetras;
        setChanged();
        notifyObserves(this, this.accName);

    }


    public float depunereNumerart(float dePus){
        setChanged();
        notifyObserves(this, this.accName);
        sum+=dePus;
        return sum;
    }

    public void afis(){
        System.out.println(accName);
    }

    public float getSum() {
        return sum;
    }

    public void setSum(float sum) {
        setChanged();
        notifyObserves(this, this.accName);
        this.sum = sum;
    }

    public String getAccName() {
        return accName;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }
}
