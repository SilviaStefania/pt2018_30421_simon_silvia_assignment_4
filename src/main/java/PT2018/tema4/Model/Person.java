package PT2018.tema4.Model;

import java.util.Observer;
import java.io.Serializable;
import java.util.Observable;

public class Person implements Observer, Serializable {


    private String name;
    private String email;


    public Person(String name, String email){
        this.name = name;
        this.email = email;

    }

    public Person()
    {

    }

    @Override
    public int hashCode() {
        int hash  = 7;
        hash = 31 * hash + name.hashCode();
        hash = 31 * hash + email.hashCode();
        System.out.println("Person "+ name+"  " +" - Computed hash: " + hash);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(this.getClass() != obj.getClass()) return false;
        Person person = (Person) obj;
        if(!name.equals(person.name )) System.out.println(name +" " + person.name);
        if (!email.equals(person.email)) System.out.println(email +" "+ person.email);
        return  !((!name.equals(person.name )) && (!email.equals(person.email))); //retrun false daca ambele sutn diferite
    }

    public void afis(){
        System.out.println(name +" "+email);
    }

    public String getName() {
        return name;
    }


    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Hello "+name+". On account "+arg+" a change has been made");
    }
}
