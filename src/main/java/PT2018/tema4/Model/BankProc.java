package PT2018.tema4.Model;

import java.io.IOException;
import java.util.ArrayList;
interface BankProc  {

   /**
    * @precondition  the list with ones account exits
    * @postcondition the returned list contains at least one element
    * @invariant
    */

   public ArrayList<Object> getPersonArrayList() throws IOException, ClassNotFoundException;

   /**
    * @precondition   the person can be created
    * @postcondition  another person will be added in the hash map
    */
   public Person addPerson(String name, String email) throws IOException;

   /**
    * @precondition   the person can be found
    * @postcondition  the person will be removed
    */
   public void removePerson (Person pii) throws IOException, ClassNotFoundException;


   /**
    * @precondition   the person can be found
    * @postcondition  the person will have another account
    * @invariant
    */
   public Account addSAVINGSAccToPers(Person p, String name, int s, boolean acTyp, float perioadaDepunere) throws IOException, ClassNotFoundException;


   /**
    * @precondition  the person can be found
    * @postcondition  all of the accounts will be found
    * @invariant
    */
   public ArrayList<Account> getSpecificPersonALLAcc(Person p) throws IOException, ClassNotFoundException;

   /**
    * @precondition   the person and the account exits
    * @postcondition  the person will no longer have that account
    */
   public void removeAccountFromPerson(Person p, Account a) throws IOException, ClassNotFoundException;

   /**
    * @precondition   the person and the account exits
    * @postcondition  the person will no longer have that account
    */
   public void getAccountData(Person p, Account a) throws IOException, ClassNotFoundException;

   /**
    * @precondition   the person and the account exit
    * @postcondition  the person will no longer have that account
    */
   public void writeAccountData(Person p, Account a, String fieldToUpdate, String newValue) throws IOException, ClassNotFoundException;

   /**
    *  @precondition   the person exits
    * @postcondition  the person will have a value modifyed
    */
   public void updatePerson(Person p, int col, String newValue) throws IOException, ClassNotFoundException;

   /**
    * @precondition
    * @postcondition
    * @invariant
    */
   public String getMess();

   /**
    * @precondition
    * @postcondition
    * @invariant
    */
   public void setMess(String mess);






 }
