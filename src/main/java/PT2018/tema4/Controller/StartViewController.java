package PT2018.tema4.Controller;

import PT2018.tema4.View.ClientView;
import PT2018.tema4.View.ProductView;
import PT2018.tema4.View.StartView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

public class StartViewController {


        private StartView view ;


        public StartViewController (StartView view) {
            this.view = view;

            this.view.addClientOppListener(new ClientButtonListener() );
            this.view.addProductOppListener(new ProdtButtonListener() );


        }

        class ClientButtonListener implements ActionListener {

            public void actionPerformed(ActionEvent arg0) {
                //cod care deschide fereastra noua de management client
                //PUNE BUTON DE BACK
                view.dispose();
                try {

                    ClientView  clientView = new ClientView();
                    clientView.setVisible(true);
                    ClientViewController clientViewController = new ClientViewController(clientView);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }





            }
        }

       class ProdtButtonListener implements ActionListener {

            public void actionPerformed(ActionEvent arg0) {
                //cod care deschide fereastra noua de management client
                //PUNE BUTON DE BACK
                view.dispose();


                try {

                    ProductView productView = new ProductView();
                    ProductViewController productViewController = new ProductViewController(productView);
                    productView.setVisible(true);
                    //ClientViewController clientViewController = new ClientViewController(clientView);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }


                //ProductViewController productViewController = new ProductViewController(productView);
            }
        }

    }





