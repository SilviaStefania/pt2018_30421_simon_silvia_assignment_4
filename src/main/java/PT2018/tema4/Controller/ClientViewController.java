package PT2018.tema4.Controller;

import PT2018.tema4.Model.Bank;
import PT2018.tema4.Model.Person;
import PT2018.tema4.View.ClientView;
import PT2018.tema4.View.StartView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;


public class ClientViewController {

        private ClientView view;

        public ClientViewController (ClientView view) {
            this.view = view;
            JTable jTable = view.getTable();

            this.view.addBackListener(new backButtonListener());
            this.view.addAddListener(new addListener());
            this.view.addDeleteListener(new deleteButtonListener());





//   DE FQACUT UPDATE DIN TABLE DIRECT

            this.view.addVIEWALLCLIENTSListener(new findAllButtonListener());

        }





    class backButtonListener implements ActionListener {

            public void actionPerformed(ActionEvent arg0) {
                //cod care deschide fereastra noua de management client
                //PUNE BUTON DE BACK
                view.dispose();
                StartView startView = new StartView();
                startView.setVisible(true);
                StartViewController startViewController = new StartViewController(startView);

            }
    }


        class addListener implements ActionListener {

            public void actionPerformed(ActionEvent arg0) {
                try {

                    Bank bank = new Bank();
                    bank.addPerson(view.getNameAddText().getText(), view.getEmailAddText().getText());
                    view.setResultAll(bank.getMess());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        class deleteButtonListener implements ActionListener {

            public void actionPerformed(ActionEvent arg0) {
                try {
                    Bank bank = new Bank();
                    int row = Integer.parseInt(view.getRow2().getText());
                    Person p = (Person) bank.getMakeObjList().get(row);
                    bank.removePerson(p);
                    view.setResultAll(bank.getMess());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }



        class findAllButtonListener implements ActionListener
        {

            public void actionPerformed(ActionEvent arg0) {
                view.dispose();
                ClientView cl = null;
                try {
                    cl = new ClientView();
                    cl.setVisible(true);
                    ClientViewController clientViewController = new ClientViewController(cl);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }




    }


