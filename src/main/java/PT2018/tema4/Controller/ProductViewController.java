package PT2018.tema4.Controller;

import PT2018.tema4.Model.Account;
import PT2018.tema4.Model.Bank;
import PT2018.tema4.Model.Person;
import PT2018.tema4.View.ProductView;
import PT2018.tema4.View.StartView;
import com.sun.javaws.security.AppContextUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class ProductViewController {

    private ProductView view;

    public ProductViewController( ProductView view){
        this.view = view;
        this.view.addBackListener(new backButtonListener());
        this.view.addDEPOZITbutton(new depozitButtonListener());
        this.view.addRETRAGEREbutton(new retr());
        this.view.addVIEWALLCLIENTSListener(new findAllButtonListener());
        this.view.addACCOUNT(new addAcccc());
        this.view.addRemoveAcc(new sterfeComnt());

    }

    class addAcccc implements  ActionListener{
        public void  actionPerformed(ActionEvent arg0) {

            try {
                Bank bank = new Bank();
                ArrayList<Object> personArrayList =  bank.getPersonArrayList();
                Iterator<Object> objectIterator = personArrayList.iterator();
                while (objectIterator.hasNext()){
                    Person p = (Person) objectIterator.next();
                    if(p.getName().equals(view.getPersName())){
                        boolean tr = false;
                        if(view.getSavings().equals("YES")){
                            tr = true;
                        }
                        bank.addSAVINGSAccToPers(p, view.getNameAcc(), view.getSumENTER(),tr, 30);


                    }
                }







            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


        }
    }


    class sterfeComnt implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {

            try {
                Bank bank = new Bank();

                ArrayList<Object> personArrayList =  bank.getPersonArrayList();
                Iterator<Object> objectIterator = personArrayList.iterator();
                while (objectIterator.hasNext()){
                    Person p = (Person) objectIterator.next();
                    if(p.getName().equals(view.getPersName())){

                        ArrayList<Account> ac = bank.getSpecificPersonALLAcc(p);
                        for(Account x: ac ){
                            if(x.getAccName().equals(view.getNameAccRemove())){
                                bank.removeAccountFromPerson(p, x);
                            }
                        }

                    }
                }

                view.setNotificareObserver(bank.getMess());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
    }


    class retr implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {

            try {
                Bank bank = new Bank();

                ArrayList<Object> personArrayList =  bank.getPersonArrayList();
                Iterator<Object> objectIterator = personArrayList.iterator();
                while (objectIterator.hasNext()){
                    Person p = (Person) objectIterator.next();
                    if(p.getName().equals(view.getPersName())){

                        ArrayList<Account> ac = bank.getSpecificPersonALLAcc(p);
                        for(Account x: ac ){
                            if(x.getAccName().equals(view.getPerformOPAC())){
                                bank.retragere(p, x, Integer.parseInt(view.getRetragereENTER().getText()));
                            }
                        }

                    }
                }

                view.setNotificareObserver(bank.getMess());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
    }



    class depozitButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {

            try {
                Bank bank = new Bank();

                ArrayList<Object> personArrayList =  bank.getPersonArrayList();
                Iterator<Object> objectIterator = personArrayList.iterator();
                while (objectIterator.hasNext()){
                    Person p = (Person) objectIterator.next();
                    if(p.getName().equals(view.getPersName())){

                        ArrayList<Account> ac = bank.getSpecificPersonALLAcc(p);
                        for(Account x: ac ){
                            if(x.getAccName().equals(view.getPerformOPAC())){
                                bank.addSum(p, x, Float.parseFloat(view.getDepositEnter().getText()));
                            }
                        }

                    }
                }

                view.setNotificareObserver(bank.getMess());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
    }




    class backButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {
            //cod care deschide fereastra noua de management client
            //PUNE BUTON DE BACK
            view.dispose();
            StartView startView = new StartView();
            startView.setVisible(true);
            StartViewController startViewController = new StartViewController(startView);

        }
    }


    class findAllButtonListener implements ActionListener
    {

        public void actionPerformed(ActionEvent arg0) {

            String pers = view.getPersName();

            try {
               Bank bank = new Bank();

                ArrayList<Object> personArrayList =  bank.getPersonArrayList();
                Iterator<Object> objectIterator = personArrayList.iterator();
                while (objectIterator.hasNext()){
                    Person p = (Person) objectIterator.next();
                    if(p.getName().equals(view.getPersName())){
                       view.updateCEVA(bank.getSpecificPersonALLAcc(p));
                    }
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            view.dispose();
            ProductView prof = null;
            try {
                prof = new ProductView();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            ProductViewController productViewController = new ProductViewController(prof);
            prof.setVisible(true);



        }
    }






}
