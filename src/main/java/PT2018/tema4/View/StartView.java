package PT2018.tema4.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class StartView extends JFrame {

        private JLabel welcomeMess = new JLabel( "Welcome!");
        private JLabel selectMess = new JLabel( "Do you want to make an operation on Person or Account?");

        private JButton clientOpButton = new JButton("Person");
        private JButton AccountOpButton = new JButton("Account");

        GridBagConstraints aranjare= new GridBagConstraints();

        public StartView (){
            JPanel calcPanel =  new JPanel(new GridBagLayout());
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setSize(500, 400);
            this.setTitle("Bank");

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 0;
            aranjare.gridx = 1;
            calcPanel.add(welcomeMess, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 1;
            aranjare.gridx = 1;
            calcPanel.add(selectMess, aranjare);


            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 3;
            aranjare.gridx = 1;
            calcPanel.add(clientOpButton, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 4;
            aranjare.gridx = 1;
            calcPanel.add(AccountOpButton, aranjare);


            this.add(calcPanel);
            this.setVisible(true);
        }


        public void addClientOppListener(ActionListener listenForClientButton){
            clientOpButton.addActionListener(listenForClientButton);
        }

        public void addProductOppListener(ActionListener listenForProdButton){
            AccountOpButton.addActionListener(listenForProdButton);
        }





    }



