package PT2018.tema4.View;

import PT2018.tema4.Model.Bank;
import PT2018.tema4.Model.Person;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClientView  extends  JFrame{

        private Person person = new Person();
        private JButton back = new JButton("Back");
        private JLabel mess1 = new JLabel("Manage Persons");
        private JButton addLable = new JButton("Add");
        private JLabel nameLable = new JLabel("Name");
        private JLabel emailLable = new JLabel("Email");
        private JButton editLable = new JButton("Edit");


        private JLabel fieldToUpLable = new JLabel("Field to update");
        private JLabel newValueLabel = new JLabel("New value");
        private JButton deleteLabel = new JButton("Delete");
        private JButton viewAllClientsButoon = new JButton("List all clients");  //cred ca mai am nevoie

          private JTextField row = new JTextField("Row",10);
         private JTextField row2 = new JTextField("Row",10);
         private JTextField col = new JTextField("Column",10);

        private JTextField nameAddText = new JTextField(10);
        private JTextField emailAddText = new JTextField(10);
        private JTextField fieldToUpTextEdit = new JTextField(10);
        private JTextField newValueTextEdit = new JTextField(10);

        private JTextField resultAll = new JTextField(20);


       Bank bank = new Bank();
       Refl refl = new Refl(bank.getPersonArrayList(),person, bank.getPersonArrayList().size(), 2);

      DefaultTableModel model = new DefaultTableModel(refl.getData(), refl.getColumnName());
      private JTable table = new JTable(model);

      private JScrollPane scrollPane = new JScrollPane(table);


      public JTable getTable() {
        return table;
    }

    public JTextField getRow2() {
        return row2;
    }

    GridBagConstraints aranjare = new GridBagConstraints();

        public ClientView() throws IOException, ClassNotFoundException {
            JPanel calcPanel =  new JPanel(new GridBagLayout());
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setSize(1500, 2000);
            this.setTitle("Clients");

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 0;
            aranjare.gridx = 0;
            calcPanel.add(back, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 2;
            aranjare.gridx = 1;
            calcPanel.add(addLable, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 1;
            aranjare.gridx = 2;
            calcPanel.add(nameLable, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 1;
            aranjare.gridx = 3;
            calcPanel.add(emailLable, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 2;
            aranjare.gridx = 2;
            calcPanel.add(nameAddText, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 2;
            aranjare.gridx = 3;
            calcPanel.add(emailAddText, aranjare);


            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 4;
            aranjare.gridx = 1;
            calcPanel.add(editLable, aranjare);


            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 3;
            aranjare.gridx = 4;
            calcPanel.add(newValueLabel, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 4;
            aranjare.gridx = 2;
            calcPanel.add(col, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 4;
            aranjare.gridx = 3;
            calcPanel.add(row, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 4;
            aranjare.gridx = 4;
            calcPanel.add(newValueTextEdit, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 6;
            aranjare.gridx = 1;
            calcPanel.add(deleteLabel, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 6;
            aranjare.gridx = 2;
            calcPanel.add(row2, aranjare);


            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 9;
            aranjare.gridx = 1;
            calcPanel.add(viewAllClientsButoon, aranjare);

            aranjare.insets = new Insets(15, 15, 15, 15);
            aranjare.gridy = 13;
            aranjare.gridx =2;
            calcPanel.add(scrollPane, aranjare);




            this.add(calcPanel);
            this.setVisible(true);


            deleteLabel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    ArrayList<Object> objects = bank.getMakeObjList();
                    Person person = (Person) objects.get(Integer.parseInt(row2.getText()));
                    try {
                        bank.removePerson(person);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (ClassNotFoundException e1) {
                        e1.printStackTrace();
                    }


                    String value  = "";

                    model.setValueAt(value, Integer.parseInt(row2.getText()), 0);
                    model.setValueAt(value, Integer.parseInt(row2.getText()), 1);


                }
            });



            editLable.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String value  = newValueTextEdit.getText();
                    ArrayList<Object> objects = bank.getMakeObjList();
                    Person person = (Person) objects.get(Integer.parseInt(row.getText()));


                    try {
                        bank.updatePerson(person, Integer.parseInt(col.getText()), value);  //////////////////////////////////////////////////////////////////
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (ClassNotFoundException e1) {
                        e1.printStackTrace();
                    }
                    model.setValueAt(value, Integer.parseInt(row.getText()), Integer.parseInt(col.getText()));

                }
            });

        }


        public JTextField getNameAddText() {
            return nameAddText;
        }

        public void setNameAddText(JTextField nameAddText) {
            this.nameAddText = nameAddText;
        }

        public JTextField getEmailAddText() {
            return emailAddText;
        }

        public void setEmailAddText(JTextField emailAddText) {
            this.emailAddText = emailAddText;
        }

        public JTextField getFieldToUpTextEdit() {
            return fieldToUpTextEdit;
        }

        public void setFieldToUpTextEdit(JTextField fieldToUpTextEdit) {
            this.fieldToUpTextEdit = fieldToUpTextEdit;
        }

        public JTextField getNewValueTextEdit() {
            return newValueTextEdit;
        }

        public void setNewValueTextEdit(JTextField newValueTextEdit) {
            this.newValueTextEdit = newValueTextEdit;
        }


        public JTextField getResultAll() {
            return resultAll;
        }

        public void setResultAll(String resultAll) {
            this.resultAll.setText(resultAll);
        }


        public void addBackListener(ActionListener listenForBackButton){
            back.addActionListener(listenForBackButton);
        }

        public void addDeleteListener(ActionListener x){
            deleteLabel.addActionListener(x);
        }

        public void addAddListener(ActionListener listenForBackButton){
            addLable.addActionListener(listenForBackButton);
        }
        public void addUPDATEListener(ActionListener x){
            editLable.addActionListener(x);
        }


        public void addVIEWALLCLIENTSListener(ActionListener x){
            viewAllClientsButoon.addActionListener(x);
        }














}
