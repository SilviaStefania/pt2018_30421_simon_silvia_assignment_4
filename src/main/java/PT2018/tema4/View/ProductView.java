package PT2018.tema4.View;

import PT2018.tema4.Model.Account;
import PT2018.tema4.Model.Bank;
import PT2018.tema4.Model.Person;
import com.sun.javaws.exceptions.BadMimeTypeResponseException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;


public class ProductView extends JFrame {


    String[] persoane = new String[100];
    JLabel deposit = new JLabel("Deposit");
    JLabel retragere = new JLabel("Retragere");
    JLabel name = new JLabel("Name");
    JLabel sum = new JLabel("Sum");
    JLabel mesaj = new JLabel("Enter the name of acc");
    JLabel savingLABEL = new JLabel("IS SAVING?");
    JLabel removeAccLable = new JLabel("Name");

    JTextField depositEnter = new JTextField(10);
    JTextField retragereENTER = new JTextField(10);
    JTextField nameENTER = new JTextField(10);
    JTextField sumENTER = new JTextField(10);
    JTextField performOPAC = new JTextField(10);
    JTextField persName = new JTextField("Pers name", 10);
    JTextField savings = new JTextField("YES", 10);
    JTextField nameAccRemove = new JTextField(10 );


    JButton depozitButton = new JButton("Deposit");
    JButton retragereButoon = new JButton("Retragere");
    JButton back = new JButton("Back");
    JButton addAcc = new JButton("Add acc");
    JButton viewAllClientsButoon = new JButton("List acc");
    JButton accRemoveButoon = new JButton("Remove");



    JLabel notificareObserver = new JLabel("");
    JLabel notificareObserver2 = new JLabel("");





    GridBagConstraints aranjare = new GridBagConstraints();

    private String[] columnName={"Account name", "Sum", "Savings?"};
    private Object[][] data = {};
    static DefaultTableModel model ;
    private JTable table = new JTable(model);
    private JScrollPane scrollPane;


    public ProductView() throws IOException, ClassNotFoundException {

        scrollPane = new JScrollPane(table);
        if (model == null) {
            scrollPane.setVisible(false);
        } else
            scrollPane.setVisible(true);


        Bank bank = new Bank();
        int i = 0;
        ArrayList<Object> personeObiecte =  bank.getPersonArrayList();
        for (Object aPersoneObiecte : personeObiecte) {
            Person p = (Person) aPersoneObiecte;
            persoane[i] = p.getName() + " " + p.getEmail();
            i++;
        }



        JPanel calcPanel =  new JPanel(new GridBagLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(2000, 1000);
        this.setTitle("Accounts");


     //   selectPers.addActionListener(this);    ASTA PUN INCONSTRUTR

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 0;
        aranjare.gridx = 0;
        calcPanel.add(back, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 1;
        calcPanel.add(persName, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 4;
        aranjare.gridx = 1;
        calcPanel.add(mesaj, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 5;
        aranjare.gridx = 1;
        calcPanel.add(performOPAC, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 3;
        calcPanel.add(deposit, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 4;
        calcPanel.add(retragere, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 8;
        calcPanel.add(savingLABEL, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 8;
        calcPanel.add(savings, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 6;
        aranjare.gridx = 1;
        calcPanel.add(viewAllClientsButoon, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 3;
        calcPanel.add(depositEnter, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 4;
        calcPanel.add(retragereENTER, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 6;
        calcPanel.add(name, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 7;
        calcPanel.add(sum, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 1;
        aranjare.gridx = 10;
        calcPanel.add(removeAccLable, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 10;
        calcPanel.add(nameAccRemove, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 3;
        aranjare.gridx = 10;
        calcPanel.add(accRemoveButoon, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 3;
        aranjare.gridx = 3;
        calcPanel.add(depozitButton, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 3;
        aranjare.gridx = 4;
        calcPanel.add(retragereButoon, aranjare);


        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 6;
        calcPanel.add(nameENTER, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 2;
        aranjare.gridx = 7;
        calcPanel.add(sumENTER, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 3;
        aranjare.gridx = 6;
        calcPanel.add(addAcc, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 4;
        aranjare.gridx = 3;
        calcPanel.add(notificareObserver, aranjare);

        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 5;
        aranjare.gridx = 3;
        calcPanel.add(notificareObserver2, aranjare);




        aranjare.insets = new Insets(15, 15, 15, 15);
        aranjare.gridy = 5;
        aranjare.gridx = 2;
        calcPanel.add(scrollPane, aranjare);


        this.add(calcPanel);
        this.setVisible(true);

    }

    public void addBackListener(ActionListener listenForBackButton){
        this.back.addActionListener(listenForBackButton);
    }

    public void addRemoveAcc(ActionListener listenForBackButton){
        this.accRemoveButoon.addActionListener(listenForBackButton);
    }




    public void addDEPOZITbutton(ActionListener x){
        depozitButton.addActionListener(x);
    }

    public void addRETRAGEREbutton(ActionListener x){
        retragereButoon.addActionListener(x);
    }


    public void addACCOUNT(ActionListener x){
        addAcc.addActionListener(x);
    }


    public void addVIEWALLCLIENTSListener(ActionListener x){
        viewAllClientsButoon.addActionListener(x);
    }


    public void setData(Object[][] data) {
        this.data = data;
    }


    public JTextField getRetragereENTER() {
        return retragereENTER;
    }

    public JTextField getDepositEnter() {
        return depositEnter;
    }


    public int getSumENTER() {
        return Integer.parseInt(sumENTER.getText());
    }

    public void setNotificareObserver(String not) {
        this.notificareObserver.setText(not);
    }

    public void setNotificareObserver2(String not) {
        this.notificareObserver2.setText(not);
    }


    public String getPerformOPAC() {
        return performOPAC.getText();
    }

    public String getPersName() {
        return persName.getText();
    }

    public String getNameAcc() {
        return nameENTER.getText();
    }

    public String getSavings() {
        return savings.getText();
    }
    public String getNameAccRemove() {
        return nameAccRemove.getText();
    }

    public void updateCEVA(ArrayList<Account> ac){
        Object[][] ceva = new Object[ac.size()][3];
        Iterator<Account> it = ac.iterator();
        int i = 0;
        while (it.hasNext()){
            Account acc= it.next();
            ceva[i][0]= acc.getAccName();
            ceva[i][1] = acc.getSum();
            ceva[i][2] = acc.getISSavin();
            i++;
        }
        model = new DefaultTableModel(ceva, columnName);


    }

}
