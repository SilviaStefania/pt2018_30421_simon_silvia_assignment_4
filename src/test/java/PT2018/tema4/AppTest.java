package PT2018.tema4;

import static org.junit.Assert.assertTrue;

import PT2018.tema4.Model.Account;
import PT2018.tema4.Model.Bank;
import PT2018.tema4.Model.Person;
import org.junit.Test;

import java.io.IOException;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void getPersArrayTEST() throws IOException, ClassNotFoundException {
        Bank bank = new Bank();
        bank.addPerson("Maria", "Maria@yahoo.com");
        bank.addPerson("Clementine", "robotz@yahoo.com");
        assertTrue(bank.getPersonArrayList().size() == 2);
    }

    @Test
    public void removePersonTEST()  throws IOException, ClassNotFoundException{
        Bank bank = new Bank();
        Person p =  bank.addPerson("Maria", "Maria@yahoo.com");
        bank.addPerson("Clementine", "robotz@yahoo.com");
        bank.removePerson( p);
        assertTrue(bank.getPersonArrayList().size() == 1);
    }

    @Test
    public void addSAVINGSTEST() throws IOException, ClassNotFoundException{
        Bank bank = new Bank();
        Person p =  bank.addPerson("Maria", "Maria@yahoo.com");
        bank.addSAVINGSAccToPers(p, "SS", 10005, true, 30);
        assertTrue(bank.getSpecificPersonALLAcc(p).size() == 1);
    }

    @Test
    public void addSPENDINGTEST() throws IOException, ClassNotFoundException{
        Bank bank = new Bank();
        Person p =  bank.addPerson("Maria", "Maria@yahoo.com");
        bank.addSAVINGSAccToPers(p, "SS", 10005, false, 30);
        assertTrue(bank.getSpecificPersonALLAcc(p).size() == 1);
    }

    @Test
    public void removeACCfromPersTEST() throws IOException, ClassNotFoundException{
        Bank bank = new Bank();
        Person p =  bank.addPerson("Maria", "Maria@yahoo.com");
        bank.addSAVINGSAccToPers(p, "SS", 10005, false, 30);
       Account ac =  bank.addSAVINGSAccToPers(p, "aa", 10005, false, 30);
       bank.removeAccountFromPerson(p, ac);
        assertTrue(bank.getSpecificPersonALLAcc(p).size() == 1);
    }


}
